import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from "@angular/common/http";
import 'rxjs/add/operator/map';

@Injectable()
export class HttpTestService {
  constructor(private _http : HttpClient) { }
       postJSON(url, body) {                
                return this._http.post(url, body, {
                  headers: new HttpHeaders({
                    'Content-Type':  'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
                    'Access-Control-Allow-Headers': 'X-Requested-With,content-type'
                  }),
                  responseType : 'text',
                  observe:'response'
                });
            }
}