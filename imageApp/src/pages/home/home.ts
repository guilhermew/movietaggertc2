import { Component } from '@angular/core';
import { NavController, LoadingController, Loading, ModalController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { HttpTestService } from '../servicos/http-service';
import { Detalhes } from '../detalhes/detalhes';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  myphoto: string;
  threshhold = 50;
  postData :  string;
  photoToSend: string;
  loading: Loading;

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, private camera: Camera, private httpService : HttpTestService,public loadingCtrl: LoadingController) {    
  
  }

  openCamera(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    
    this.camera.getPicture(options).then((imageData) => {
      
      this.myphoto = 'data:image/jpeg;base64,' + imageData;
      this.photoToSend = imageData;    
     
    }, (err) => {
     // Handle error
    });
  }

  reconhecer(){ 
    this.loading = this.loadingCtrl.create({
      content: 'Buscando o filme. Aguarde...',
      dismissOnPageChange: true
    });
    this.loading.present();
    //let url = "http://10.0.2.2:8080/api/predict/";
    let url = "http://192.168.101.9:8080/api/predict/";
    let data = {
      image: this.photoToSend,
      threshold: this.threshhold
    }

    this.httpService.postJSON(url, JSON.stringify(data))
    .subscribe(response => {
      this.loading.dismiss();
      console.log(JSON.parse(response.body));
      let modal = this.modalCtrl.create(Detalhes, {params:response.body});
      modal.present();
    },
       error => this.loading.dismiss(),
       () => console.log("acesso a webapi post ok...")
    );
}

}
