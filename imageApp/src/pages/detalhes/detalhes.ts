import { Component } from '@angular/core';
import { NavController, LoadingController, Loading, ModalController, NavParams, ViewController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { HttpTestService } from '../servicos/http-service';


@Component({
  selector: 'detalhes',
  templateUrl: 'detalhes.html'
})
export class Detalhes {

  filmes = [];

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams) {   
    JSON.parse(navParams.get('params')).forEach(filme => {
      filme.simmilarity = (filme.simmilarity*100).toFixed(2);      
      filme.movieInfo.poster = 'data:image/jpeg;base64,' + filme.movieInfo.poster;
      this.filmes.push(filme);
    });
  }
 
  closeModal(){
    this.viewCtrl.dismiss();
  }
}